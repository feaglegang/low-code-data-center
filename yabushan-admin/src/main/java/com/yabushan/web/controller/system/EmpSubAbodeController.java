package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubAbode;
import com.yabushan.system.service.IEmpSubAbodeService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工家庭住址子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/abode")
public class EmpSubAbodeController extends BaseController
{
    @Autowired
    private IEmpSubAbodeService empSubAbodeService;

    /**
     * 查询员工家庭住址子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:abode:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubAbode empSubAbode)
    {
        startPage();
        List<EmpSubAbode> list = empSubAbodeService.selectEmpSubAbodeList(empSubAbode);
        return getDataTable(list);
    }

    /**
     * 导出员工家庭住址子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:abode:export')")
    @Log(title = "员工家庭住址子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubAbode empSubAbode)
    {
        List<EmpSubAbode> list = empSubAbodeService.selectEmpSubAbodeList(empSubAbode);
        ExcelUtil<EmpSubAbode> util = new ExcelUtil<EmpSubAbode>(EmpSubAbode.class);
        return util.exportExcel(list, "abode");
    }

    /**
     * 获取员工家庭住址子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:abode:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubAbodeService.selectEmpSubAbodeById(recId));
    }

    /**
     * 新增员工家庭住址子集
     */
    @PreAuthorize("@ss.hasPermi('system:abode:add')")
    @Log(title = "员工家庭住址子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubAbode empSubAbode)
    {
        return toAjax(empSubAbodeService.insertEmpSubAbode(empSubAbode));
    }

    /**
     * 修改员工家庭住址子集
     */
    @PreAuthorize("@ss.hasPermi('system:abode:edit')")
    @Log(title = "员工家庭住址子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubAbode empSubAbode)
    {
        return toAjax(empSubAbodeService.updateEmpSubAbode(empSubAbode));
    }

    /**
     * 删除员工家庭住址子集
     */
    @PreAuthorize("@ss.hasPermi('system:abode:remove')")
    @Log(title = "员工家庭住址子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubAbodeService.deleteEmpSubAbodeByIds(recIds));
    }
}
