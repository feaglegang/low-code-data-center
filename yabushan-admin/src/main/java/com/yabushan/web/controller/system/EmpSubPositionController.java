package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubPosition;
import com.yabushan.system.service.IEmpSubPositionService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工从事岗位子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/position")
public class EmpSubPositionController extends BaseController
{
    @Autowired
    private IEmpSubPositionService empSubPositionService;

    /**
     * 查询员工从事岗位子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:position:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubPosition empSubPosition)
    {
        startPage();
        List<EmpSubPosition> list = empSubPositionService.selectEmpSubPositionList(empSubPosition);
        return getDataTable(list);
    }

    /**
     * 导出员工从事岗位子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:position:export')")
    @Log(title = "员工从事岗位子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubPosition empSubPosition)
    {
        List<EmpSubPosition> list = empSubPositionService.selectEmpSubPositionList(empSubPosition);
        ExcelUtil<EmpSubPosition> util = new ExcelUtil<EmpSubPosition>(EmpSubPosition.class);
        return util.exportExcel(list, "position");
    }

    /**
     * 获取员工从事岗位子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:position:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubPositionService.selectEmpSubPositionById(recId));
    }

    /**
     * 新增员工从事岗位子集
     */
    @PreAuthorize("@ss.hasPermi('system:position:add')")
    @Log(title = "员工从事岗位子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubPosition empSubPosition)
    {
        return toAjax(empSubPositionService.insertEmpSubPosition(empSubPosition));
    }

    /**
     * 修改员工从事岗位子集
     */
    @PreAuthorize("@ss.hasPermi('system:position:edit')")
    @Log(title = "员工从事岗位子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubPosition empSubPosition)
    {
        return toAjax(empSubPositionService.updateEmpSubPosition(empSubPosition));
    }

    /**
     * 删除员工从事岗位子集
     */
    @PreAuthorize("@ss.hasPermi('system:position:remove')")
    @Log(title = "员工从事岗位子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubPositionService.deleteEmpSubPositionByIds(recIds));
    }
}
