package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxOrderGiftInfo;
import com.yabushan.system.service.IYmxOrderGiftInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 订单礼物Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxordergiftinfo")
public class YmxOrderGiftInfoController extends BaseController
{
    @Autowired
    private IYmxOrderGiftInfoService ymxOrderGiftInfoService;

    /**
     * 查询订单礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxOrderGiftInfo ymxOrderGiftInfo)
    {
        startPage();
        List<YmxOrderGiftInfo> list = ymxOrderGiftInfoService.selectYmxOrderGiftInfoList(ymxOrderGiftInfo);
        return getDataTable(list);
    }

    /**
     * 导出订单礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:export')")
    @Log(title = "订单礼物", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxOrderGiftInfo ymxOrderGiftInfo)
    {
        List<YmxOrderGiftInfo> list = ymxOrderGiftInfoService.selectYmxOrderGiftInfoList(ymxOrderGiftInfo);
        ExcelUtil<YmxOrderGiftInfo> util = new ExcelUtil<YmxOrderGiftInfo>(YmxOrderGiftInfo.class);
        return util.exportExcel(list, "ymxordergiftinfo");
    }

    /**
     * 获取订单礼物详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:query')")
    @GetMapping(value = "/{orderGiftId}")
    public AjaxResult getInfo(@PathVariable("orderGiftId") String orderGiftId)
    {
        return AjaxResult.success(ymxOrderGiftInfoService.selectYmxOrderGiftInfoById(orderGiftId));
    }

    /**
     * 新增订单礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:add')")
    @Log(title = "订单礼物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxOrderGiftInfo ymxOrderGiftInfo)
    {
        return toAjax(ymxOrderGiftInfoService.insertYmxOrderGiftInfo(ymxOrderGiftInfo));
    }

    /**
     * 修改订单礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:edit')")
    @Log(title = "订单礼物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxOrderGiftInfo ymxOrderGiftInfo)
    {
        return toAjax(ymxOrderGiftInfoService.updateYmxOrderGiftInfo(ymxOrderGiftInfo));
    }

    /**
     * 删除订单礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxordergiftinfo:remove')")
    @Log(title = "订单礼物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderGiftIds}")
    public AjaxResult remove(@PathVariable String[] orderGiftIds)
    {
        return toAjax(ymxOrderGiftInfoService.deleteYmxOrderGiftInfoByIds(orderGiftIds));
    }
}
