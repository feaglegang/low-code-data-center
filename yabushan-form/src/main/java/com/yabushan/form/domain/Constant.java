package com.yabushan.form.domain;

import java.util.Map;

public enum Constant {

    NGZ(5,5,5,500,0,5),
    NGM(5,5,5,5000,0,10),
    NGT(10,10,10,50000,0,10),
    NGH(20,20,20,100000,1,20),
    NGS(50,50,50,500000,5,50);


    /**
     * 聚合数据API数量
     */
    Integer JH_API_NUM;
    /**
     * 建表数量
     */
    Integer CREATE_TABLE_NUM;
    /**
     * 一个表最多创建字段数
     */
    Integer ONE_TABLE_FILED_NUM;
    /**
     * 一个表最多行数
     */
    Integer ONE_TABLE_ROW_NUM;
    /**
     * 自定义数据库数量
     */
    Integer DATASOURCE_CUSTOMER;
    /**
     * 允许创建API数量
     */
    Integer API_NUM;

    Constant(Integer JH_API_NUM, Integer CREATE_TABLE_NUM, Integer ONE_TABLE_FILED_NUM, Integer ONE_TABLE_ROW_NUM, Integer DATASOURCE_CUSTOMER, Integer API_NUM) {
        this.JH_API_NUM = JH_API_NUM;
        this.CREATE_TABLE_NUM = CREATE_TABLE_NUM;
        this.ONE_TABLE_FILED_NUM = ONE_TABLE_FILED_NUM;
        this.ONE_TABLE_ROW_NUM = ONE_TABLE_ROW_NUM;
        this.DATASOURCE_CUSTOMER = DATASOURCE_CUSTOMER;
        this.API_NUM = API_NUM;
    }

    public Integer getJH_API_NUM() {
        return JH_API_NUM;
    }

    public Integer getCREATE_TABLE_NUM() {
        return CREATE_TABLE_NUM;
    }

    public Integer getONE_TABLE_FILED_NUM() {
        return ONE_TABLE_FILED_NUM;
    }

    public Integer getONE_TABLE_ROW_NUM() {
        return ONE_TABLE_ROW_NUM;
    }

    public Integer getDATASOURCE_CUSTOMER() {
        return DATASOURCE_CUSTOMER;
    }

    public Integer getAPI_NUM() {
        return API_NUM;
    }
}
