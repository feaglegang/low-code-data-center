import request from '@/utils/request'

// 查询客户信息列表
export function listYmxinformationinfo(query) {
  return request({
    url: '/ymx/ymxinformationinfo/list',
    method: 'get',
    params: query
  })
}

// 查询客户信息详细
export function getYmxinformationinfo(customerInformationId) {
  return request({
    url: '/ymx/ymxinformationinfo/' + customerInformationId,
    method: 'get'
  })
}

// 新增客户信息
export function addYmxinformationinfo(data) {
  return request({
    url: '/ymx/ymxinformationinfo',
    method: 'post',
    data: data
  })
}

// 修改客户信息
export function updateYmxinformationinfo(data) {
  return request({
    url: '/ymx/ymxinformationinfo',
    method: 'put',
    data: data
  })
}

// 删除客户信息
export function delYmxinformationinfo(customerInformationId) {
  return request({
    url: '/ymx/ymxinformationinfo/' + customerInformationId,
    method: 'delete'
  })
}

// 导出客户信息
export function exportYmxinformationinfo(query) {
  return request({
    url: '/ymx/ymxinformationinfo/export',
    method: 'get',
    params: query
  })
}