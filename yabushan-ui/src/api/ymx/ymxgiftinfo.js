import request from '@/utils/request'

// 查询礼物列表
export function listYmxgiftinfo(query) {
  return request({
    url: '/ymx/ymxgiftinfo/list',
    method: 'get',
    params: query
  })
}

// 查询礼物详细
export function getYmxgiftinfo(giftId) {
  return request({
    url: '/ymx/ymxgiftinfo/' + giftId,
    method: 'get'
  })
}

// 新增礼物
export function addYmxgiftinfo(data) {
  return request({
    url: '/ymx/ymxgiftinfo',
    method: 'post',
    data: data
  })
}

// 修改礼物
export function updateYmxgiftinfo(data) {
  return request({
    url: '/ymx/ymxgiftinfo',
    method: 'put',
    data: data
  })
}

// 删除礼物
export function delYmxgiftinfo(giftId) {
  return request({
    url: '/ymx/ymxgiftinfo/' + giftId,
    method: 'delete'
  })
}

// 导出礼物
export function exportYmxgiftinfo(query) {
  return request({
    url: '/ymx/ymxgiftinfo/export',
    method: 'get',
    params: query
  })
}